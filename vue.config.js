/*
 * @Author: your name
 * @Date: 2020-06-06 10:19:30
 * @LastEditTime: 2021-03-23 16:52:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YoungLaw_Vue_Member\vue.config.js
 */

const TimeStamp = 'JIEDIAN'
module.exports = {
  pages: {
    index: {
      // page 的入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      // 在 dist/index.html 的输出
      filename: 'index.html',
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: '智慧用电',
      // 在这个页面中包含的块，默认情况下会包含
      // 提取出来的通用 chunk 和 vendor chunk。
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },
  lintOnSave: true,
  publicPath: './',
  outputDir: 'JIEDIAN',
  assetsDir: 'static',
  productionSourceMap: false,
  configureWebpack: {
    output: { // 输出重构  打包编译后的 文件名称  【模块名称.版本号.js】
      filename: `js/[name].${TimeStamp}.js`,
      chunkFilename: `js/[name].${TimeStamp}.js`
    }
    // config => {
    //   if (process.env.NODE_ENV === 'production') {
    //     config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    //   }
    //   config.externals = {
    //     AMap: 'AMap' // 高德地图配置
    //   }
    // }
  },
  // 修改打包后img文件名
  chainWebpack: config => {
    config.module
      .rule('images')
      .use('url-loader')
      .tap(options => {
        return {
          limit: 4096,
          fallback: {
            loader: 'file-loader',
            options: {
              name: `img/[name].${TimeStamp}.[ext]`
            }
          }
        }
      })
  },
  devServer: {
    open: true,
    disableHostCheck: true
  },

  pwa: {
    themeColor: '#1117E0',
    msTileColor: '#331C1C',
    manifestOptions: {
      background_color: '#629FFD'
    }
  }
  // webpackConfig: {

  // }
}
