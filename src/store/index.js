/*
 * @Author: your name
 * @Date: 2020-07-30 15:44:11
 * @LastEditTime: 2020-08-07 14:56:44
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \YoungLaw_App_Vue_Member\src\store\index.js
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {}
})
