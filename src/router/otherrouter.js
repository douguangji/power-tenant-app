/*
 * @Author: your name
 * @Date: 2021-03-19 15:53:30
 * @LastEditTime: 2021-03-19 15:54:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \app\src\router\otherrouter.js
 */
const otherrouter = [
  {
    path: '/demo',
    name: 'demo',
    component: resolve => require(['@/views/demo.vue'], resolve)
  },
  {
    path: '/gaodemap',
    name: 'gaodemap',
    component: resolve => require(['@/views/gaodemap.vue'], resolve)
  },
  {
    path: '/404',
    name: '404',
    component: resolve => require(['@/views/404.vue'], resolve)
  },
  {
    path: '/login',
    name: 'login',
    component: resolve => require(['@/views/Personalcenter/login/login.vue'], resolve)
  }
]
export default otherrouter
