/*
 * @Author: your name
 * @Date: 2021-03-19 11:30:40
 * @LastEditTime: 2021-03-19 15:55:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \app\src\router\home.js
 */
const homerouter = [
  // 首页
  {
    path: '/',
    name: 'Home',
    component: resolve => require(['@/views/Home/Home.vue'], resolve),
    meta: {
      requireAuth: true, // 是否需要权限校验
      keepAlive: false // 需要缓存
    }
  },
  {
    path: '/Conlist',
    name: 'Conlist',
    component: resolve => require(['../views/Home/conlist/Conlist.vue'], resolve),
    meta: {
      requireAuth: true, // 权限的校验
      keepAlive: false // 需要缓存
    }
  }
]

export default homerouter
