/*
 * @Author: your name
 * @Date: 2021-03-19 11:30:40
 * @LastEditTime: 2021-03-23 11:58:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \app\src\router\home.js
 */
const Personal = [
  {
    path: '/choospass',
    name: 'choospass',
    component: resolve => require(['@/views/Personalcenter/choospass/choospass.vue'], resolve)
  },
  {
    path: '/userinfo',
    name: 'Userinfo',
    component: resolve => require(['@/views/Personalcenter/userinnfo/index.vue'], resolve)
  },
  {
    path: '/Personalcenter',
    name: 'Personalcenter',
    component: resolve => require(['@/views/Personalcenter/Personalcenter.vue'], resolve),
    meta: {
      requireAuth: true, // 是否需要权限校验
      keepAlive: false // 需要缓存
    }
  }
]

export default Personal
