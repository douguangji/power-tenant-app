/*
 * @Author: your name
 * @Date: 2020-07-22 11:41:26
 * @LastEditTime: 2021-03-19 15:54:45
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \memberapp\src\router\index.js
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import homerouter from './home.js'
import Personal from './Personalcenter'
import otherrouter from './otherrouter'

Vue.use(VueRouter)

const routes = [...otherrouter, ...homerouter, ...Personal]
const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
