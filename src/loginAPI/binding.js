/*
 * @Author: your name
 * @Date: 2020-08-06 21:12:33
 * @LastEditTime: 2021-03-23 16:02:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YoungLaw_App_Vue_Member\src\loginAPI\binding.js
 */
import { RiestCallServerPost } from './../utils/RiestPubmemberCallServer'

// 绑定学生
function Bindingstudents(data) {
  return RiestCallServerPost({
    url: '/binding/student',
    data: data
  })
}

// 绑定家长
function Bindingparents(data) {
  return RiestCallServerPost({
    url: '/binding/parent',
    data: data
  })
}

// 绑定教师
function Bindingteacher(data) {
  return RiestCallServerPost({
    url: '/binding/teacher',
    data: data
  })
}

// 绑定管理平台用户
function Bindingtenant(data) {
  return RiestCallServerPost({
    url: '/binding/tenantuser',
    data: data
  })
}

// 选择学校
function selectschool(data) {
  const systemparam = {
    // showsuccess > 0 显示成功弹框
    showsuccess: 0,
    // showerror > 0 显示失败弹框
    showerror: 0,
    // showloading > 0 显示loading遮罩层
    showloading: 0
  }
  const dictdata = { ...data, ...systemparam }
  return RiestCallServerPost({
    url: '/con/select',
    data: dictdata
  })
}

// 获取绑定的手机号
function slephone() {
  return RiestCallServerPost({
    url: '/binding/getloginmphone',
    data: {}
  })
}

// 解除绑定
function removebinding(data) {
  return RiestCallServerPost({
    url: '/binding/removebinding',
    data: data
  })
}

export default {
  Bindingstudents,
  Bindingparents,
  Bindingteacher,
  Bindingtenant,
  selectschool,
  slephone,
  removebinding
}
