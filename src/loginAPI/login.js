/*
 * @Author: your name
 * @Date: 2020-08-06 21:12:33
 * @LastEditTime: 2021-03-23 13:39:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YoungLaw_App_Vue_Member\src\loginAPI\login.js
 */
import { RiestCallServerPost } from './../utils/RiestPubmemberCallServer'

// 登录
function login(data) {
  const systemparam = {
    // showsuccess > 0 显示成功弹框
    showsuccess: 1,
    // showerror > 0 显示失败弹框
    showerror: 1,
    // showloading > 0 显示loading遮罩层
    showloading: 1
  }
  const dictdata = { ...data, ...systemparam }
  return RiestCallServerPost({
    url: '/login',
    data: dictdata
  })
}

// 登录
function AppTokenLogin(data) {
  return RiestCallServerPost({
    url: '/tokenlogin',
    data: data
  })
}

// 登出
function loginout() {
  return RiestCallServerPost({
    url: '/logout',
    data: {}
  })
}

// 发送验证码
function riestcode(data) {
  const systemparam = {
    // showsuccess > 0 显示成功弹框
    showsuccess: 1,
    // showerror > 0 显示失败弹框
    showerror: 1,
    // showloading > 0 显示loading遮罩层
    showloading: 1
  }
  const dictdata = { ...data, ...systemparam }
  return RiestCallServerPost({
    url: '/riest/sms/phonelogin/code',
    data: dictdata
  })
}

// 获取登陆后的school列表
function schoollist() {
  return RiestCallServerPost({
    url: '/schoollist',
    data: {}
  })
}

function presentMphone(data) {
  return RiestCallServerPost({
    url: '/user/presentmphone',
    data: data
  })
}

function newMphone(data) {
  return RiestCallServerPost({
    url: '/user/newmphone',
    data: data
  })
}

function presentcode(data) {
  return RiestCallServerPost({
    url: '/user/presentcode',
    data: data
  })
}

function QueryTime(query) {
  const systemparam = {
    // showsuccess > 0 显示成功弹框
    showsuccess: 0,
    // showerror > 0 显示失败弹框
    showerror: 0,
    // showloading > 0 显示loading遮罩层
    showloading: 0
  }
  const data = { ...query, ...systemparam }
  return RiestCallServerPost({
    url: '/user/querytime',
    data: data
  })
}

export default {
  login,
  AppTokenLogin,
  loginout,
  riestcode,
  schoollist,
  presentMphone,
  newMphone,
  presentcode,
  QueryTime
}
