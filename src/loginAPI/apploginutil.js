import {
  SaveRiestClientDataLocalStorage,
  GetRiestClientDataLocalStorage,
  RemoveRiestClientDataLocalStorage,
  SaveRiestClientDataSessionStorage
} from '@/utils/RiestStorage'
import RiestUtil from '@/utils/riestutil.js'

/**
 * 保存APP登录信息
 * @param {*} data
 * @param {*} loginparam
 */
export function SaveAppLoginRetdata(data, loginparam) {
  var curlogin = {} // 当前账号登录信息
  curlogin.apptoken = data.retdata.token
  curlogin.commemberid = data.retdata.commemberid
  curlogin.username = data.retdata.username
  curlogin.loginid = loginparam.mphone
  // curlogin.companyname = loginparam.companyname
  if (RiestUtil.is_define(data.retdata.canGzhLogin) && data.retdata.canGzhLogin === 1) {
    curlogin.showusername = data.retdata.username + '(微信)'
    curlogin.logintype = 2
  } else {
    curlogin.showusername = data.retdata.username + '(账号登录)'
    curlogin.logintype = 1
  }
  RiestSaveLoginInfoList(curlogin, true)

  // 是否启用极光推送
  //   if (is_define(EnableInitJPush) && EnableInitJPush == true) {
  //     sdk_j_uid_set(data.retdata.pushuserid)
  //   }

  SaveRiestClientDataLocalStorage('LoginReturnData', JSON.stringify(data))
  SaveRiestClientDataLocalStorage('tenantdomainname', loginparam.companyname)
  SaveRiestClientDataSessionStorage('WxGzhDemoShowToast', 0)
}

// 登录信息类操作
export function RiestGetLoginInfoList() {
  var loginjson = GetRiestClientDataLocalStorage('logininfo-list')
  if (loginjson == null || loginjson.length < 1) {
    loginjson = []
    return loginjson
  }
  return JSON.parse(loginjson)
}

/*
 * 存储登录列表
 * iscurrent 是否设置为当前登录用户
 */
export function RiestSaveLoginInfoList(logindata, iscurrent) {
  // 参数：apptoken,tenantuserid,loginid,companyname
  // logintype  1 apptoken   2 weixin unionid  3 phone login
  var loginjson = RiestGetLoginInfoList()
  var key = logindata.username + '-' + logindata.commemberid + '-' + logindata.logintype
  // 查找
  for (var i = 0; i < loginjson.length; i++) {
    var item = loginjson[i]
    // console.log('save item key:'+item.key);
    // console.log('save key     :'+key);
    if (item.key === key) {
      // 替换
      loginjson[i].apptoken = logindata.apptoken
      loginjson[i].commemberid = logindata.commemberid
      loginjson[i].loginid = logindata.loginid
      // loginjson[i].companyname = logindata.companyname
      loginjson[i].showusername = logindata.showusername
      loginjson[i].logintype = logindata.logintype
      loginjson[i].username = logindata.username

      // console.log('save start logininfo-list:'+JSON.stringify(loginjson));

      SaveRiestClientDataLocalStorage('logininfo-list', JSON.stringify(loginjson))

      // console.log('save end logininfo-list:'+GetRiestClientDataLocalStorage('logininfo-list'));

      if (iscurrent !== undefined && iscurrent != null && iscurrent === true) {
        SaveRiestClientDataLocalStorage('logininfo-current', JSON.stringify(item))
      }
      return
    }
  }

  // 没有找到插入
  var insertdata = {}
  insertdata.key = key
  insertdata.apptoken = logindata.apptoken
  insertdata.commemberid = logindata.commemberid
  insertdata.loginid = logindata.loginid
  // insertdata.companyname = logindata.companyname
  insertdata.showusername = logindata.showusername
  insertdata.logintype = logindata.logintype
  insertdata.username = logindata.username

  loginjson.push(insertdata)
  SaveRiestClientDataLocalStorage('logininfo-list', JSON.stringify(loginjson))
  if (iscurrent !== undefined && iscurrent != null && iscurrent === true) {
    SaveRiestClientDataLocalStorage('logininfo-current', JSON.stringify(insertdata))
  }
}

export function RiestRemoveLoginInfoCurrent() {
  var logincur = RiestGetLoginInfoCurrent()
  if (logincur == null) { return }

  RemoveRiestClientDataLocalStorage('logininfo-current')

  var loginjson = RiestGetLoginInfoList()
  // 查找
  for (var i = 0; i < loginjson.length; i++) {
    var item = loginjson[i]
    if (item.key === logincur.key) {
      // delete
      loginjson.splice(i, 1)
      SaveRiestClientDataLocalStorage('logininfo-list', JSON.stringify(loginjson))
      return
    }
  }
}

export function RiestRemoveLoginInfoByKey(key) {
  var logincur = RiestGetLoginInfoCurrent()
  if (logincur != null && logincur.key === key) {
    RiestRemoveLoginInfoCurrent()
    return
  }

  var loginjson = RiestGetLoginInfoList()
  // 查找
  for (var i = 0; i < loginjson.length; i++) {
    var item = loginjson[i]
    if (item.key === key) {
      // delete
      loginjson.splice(i, 1)
      SaveRiestClientDataLocalStorage('logininfo-list', JSON.stringify(loginjson))
      return
    }
  }
}
export function RiestRemoveLoginInfoAll() {
  RiestRemoveLoginInfoCurrent()
  SaveRiestClientDataLocalStorage('logininfo-list', '')
}
export function RiestRemoveLoginInfoAllByType(type) {
  var logincur = RiestGetLoginInfoCurrent()
  if (logincur != null && logincur.logintype === type) {
    RiestRemoveLoginInfoCurrent()
  }
  var loginjson = RiestGetLoginInfoList()
  // 查找
  for (var i = 0; i < loginjson.length; i++) {
    var item = loginjson[i]
    if (item.logintype === type) {
      // delete
      loginjson.splice(i, 1)
      i--// 删除后i发生变化
    }
  }
  SaveRiestClientDataLocalStorage('logininfo-list', JSON.stringify(loginjson))
}

export function RiestGetLoginInfoCurrent() {
  var logincur = GetRiestClientDataLocalStorage('logininfo-current')

  if (logincur === undefined || logincur == null || logincur.length < 1) { return null }
  return JSON.parse(logincur)
}
export function RiestGetLoginInfoByKey(key) {
  var logincur = RiestGetLoginInfoCurrent()
  if (logincur != null && logincur.key === key) {
    return logincur
  }

  var loginjson = RiestGetLoginInfoList()
  // 查找
  for (var i = 0; i < loginjson.length; i++) {
    var item = loginjson[i]
    if (item.key === key) {
      return item
    }
  }
  return null
}

