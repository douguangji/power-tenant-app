import RiestUtil from '@/utils/riestutil.js'
import { RiestGetLoginInfoCurrent } from './apploginutil.js'
import { setToken, setUser, setschool, removeToken,
  removeUser,
  removeschool,
  removerolestypeid,
  removetentschoolid,
  removerole,
  setlogintype,
  // getlogintype,
  removelogintype
} from '@/utils/auth'
import { RemoveRiestClientDataLocalStorage,
  SaveRiestClientDataLocalStorage } from './../utils/RiestStorage'
import login from './login.js'
import { RiestMessage } from './../utils/message.js'

/**
 * APPToken登录
 */
export function AppTokenLogin() {
  var logincur = RiestGetLoginInfoCurrent()
  console.log('login', logincur)
  if (!RiestUtil.is_define(logincur) || logincur.logintype !== 1) {
    // token登录失败后跳转登录页
    // RiestOpenPage('', 'login.html', '', 0, false)
    return
  }

  var apptoken = logincur.apptoken
  var commemberid = logincur.commemberid

  var imei = RiestUtil.RiestGetDeviceID()
  console.warn('imei = ', imei)
  var param = {
    apptoken: apptoken,
    imei: imei,
    commemberid: commemberid
  }

  login.AppTokenLogin(param).then(res => {
    //   // 是否启用极光推送
    //   if (RiestUtil.is_define(EnableInitJPush) && EnableInitJPush) {
    //     sdk_j_uid_set(data.retdata.pushuserid)
    //   }
    if (res.riestcode === 0) {
      console.warn('apptoken登录成功')
      console.warn('apptoken登录成功', res)
      setToken(res.jwttoken)
      setUser(JSON.stringify(res.retdata))
      setschool(JSON.stringify(res.retdata.schoollist))
      setlogintype(1)
      //   SavePopedomJson(data.retdata.userpopedom.userpopedomlist)
      SaveRiestClientDataLocalStorage('LoginReturnData', JSON.stringify(res))
      //   RiestOpenPage('', 'index.html', '', 0, false)
    }
  }).catch((err) => {
    console.log(err)
    console.warn('apptoken登录成功失敗')
    RiestMessage('登录失效，请重新登录')
    removeToken()
    removeUser()
    removeschool()
    removerolestypeid()
    removerole()
    removetentschoolid()
    removelogintype()
    RemoveRiestClientDataLocalStorage('logininfo-current')
    location.reload()
  })
}
