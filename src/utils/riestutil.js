/*
 * @Author: your name
 * @Date: 2020-06-22 18:14:23
 * @LastEditTime: 2020-07-22 18:46:41
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YoungLaw_Vue_Tenant\src\utils\is_define.js
 */
/**
 * 判断是否是空
 * @param value
 */
// eslint-disable-next-line no-unused-vars
function is_define(value) {
  if (
    value == null ||
    value === '' ||
    value === 'undefined' ||
    value === undefined ||
    value === 'null' ||
    value === '(null)' ||
    value === 'NULL' ||
    typeof value === 'undefined'
  ) {
    return false
  } else {
    value = value + ''
    value = value.replace(/\s/g, '')
    if (value === '') {
      return false
    }
    return true
  }
}
/**
 * 检查当前页面是否是手机
 */
function isMobile() {
  // 获取设备型号
  var DeviceInfo = navigator.userAgent

  // console.log(DeviceInfo)

  // 常用移动设备型号   //PC   Windows/Mac
  var mobileList = ['Android', 'iPhone', 'iPad', 'iPod']

  var mobile = false

  for (var v = 0; v < mobileList.length; v++) {
    if (DeviceInfo.indexOf(mobileList[v]) > 0) {
      mobile = true
      break
    }
  }
  // console.log("是否是mobile=="+mobile)
  return mobile
}

function RiestGetDeviceID() {
  var retstr
  const globaltype = process.env.VUE_APP_globaltype
  const isWeiXinGZH = process.env.VUE_APP_isWeiXinGZH
  // apicloud
  if (parseInt(globaltype) === 2) {
    if (isWeiXinGZH) {
      retstr = 'GZH-TEMP-IMEI'
    } else {
      retstr = window.api.deviceId
    }
    return retstr
  } else {
    retstr = 'VUE-PAGE-TEMP-IMEI'
    return retstr
  }
}
export default {
  is_define,
  isMobile,
  RiestGetDeviceID
}
