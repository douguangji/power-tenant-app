/* eslint-disable no-undef */
/* eslint-disable no-self-assign */
import axios from 'axios'
import qs from 'qs'

axios.defaults.timeout = 10000;                        //响应时间
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';        //配置请求头
axios.defaults.baseURL = 'http://test.jxhd.heweixiao.cn/javaservice';   //配置接口地址
//axios.defaults.baseURL = 'http://106.12.71.45:8080';   //配置接口地址

//POST传参序列化(添加请求拦截器)
axios.interceptors.request.use((config) => {
    isShowLoading(true);
  //在发送请求之前做某件事
  if(config.method  === 'post'){
    config.data = qs.stringify(config.data);
  }
  return config;
},(error) =>{
  console.log('错误的传参')
  return Promise.reject(error);
});

//返回状态判断(添加响应拦截器)
axios.interceptors.response.use((res) =>{
  //对响应数据做些事
  isShowLoading(false);
  if (res.riestcode !== 0) {
    if (res.riestcode === 9999) {
      // to re-login
      MessageBox.confirm('您已经注销，您可以取消以停留在此页面，或再次登录', '登录已过期', {
        confirmButtonText: '重新登入',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {
        store.dispatch('user/resetToken').then(() => {
          location.reload()
        })
      })
    } else if (res.riestcode === 9998) {
      // to re-login
      const msg = '您未配置权限：' + res.riestmsg
      MessageBox.confirm(msg, '确认错误', {
        confirmButtonText: '我知道了',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(() => {

      })
    } else {
      return res
    }
    // if (res.riestcode === 1) {
    //   Message({
    //     message: res.riestmsg || 'error',
    //     type: 'error',
    //     duration: 5 * 1000
    //   })
    // }
    // return Promise.reject(res.message || 'error')
  } else {
    return res
  }
}, (error) => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
  return Promise.reject(error);
});

//返回一个Promise(发送post请求)
export function fetchPost(url, params) {
  return new Promise((resolve, reject) => {
    axios.post(url, params)
      .then(response => {
        resolve(response);
      }, err => {
        reject(err);
      })
      .catch((error) => {
        reject(error)
      })
  })
}
////返回一个Promise(发送get请求)
export function fetchGet(url, param) {
  return new Promise((resolve, reject) => {
    axios.get(url, {params: param})
      .then(response => {
        resolve(response)
      }, err => {
        reject(err)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
export default {
  fetchPost,
  fetchGet,
}
