/*
 * @Author: your name
 * @Date: 2020-07-22 14:24:59
 * @LastEditTime: 2020-07-22 16:24:32
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \memberapp\src\utils\RiestBaseDownloadCallServer.js
 */
import {
  RiestBaseCallServer,
  BaseCallShowMsg
} from '@/utils/RiestBaseCallServer'

export function RiestBaseDownloadCallServer(req) {
  return new Promise((resolve, reject) => {
    RiestBaseCallServer(req)
      .then(response => {
        const content = response.data
        const blob = new Blob([content])

        if (response.data.type === 'application/json') {
          new Promise((t_resolve, t_reject) => {
            const reader = new FileReader()
            console.log('reader', reader)
            console.log('reader.onload', reader.onload)
            reader.onload = function(e) {
              console.log('reader-e', e)
              const obj = JSON.parse(e.target.result)
              console.log('reader-obj', obj)
              if (obj) {
                response.data = obj
                // 当前消息只代表处理返回信息处理成功
                t_resolve({ riestcode: 0, riestmsg: 'success' })
              } else {
                t_reject({ riestcode: 1, riestmsg: '下载失败' })
              }
            }
            console.log('text-blob', reader.readAsText(blob))
          }).then(() => {
            console.log('reader-obj-response.data', response.data)
            const res = response.data

            // 请求响应弹框
            BaseCallShowMsg(
              req,
              res,
              success => {
                resolve(success)
              },
              fail => {
                reject(fail)
              }
            )
          })
        } else {
          // 获取heads中的filename文件名
          const tempfileName = response.headers['content-disposition']
            .split(';')[1]
            .split('fileName=')[1]
          // 使用decodeURI对后端编码后的文件名进行解码
          const fileName = decodeURI(tempfileName)
          console.log('fileName_', fileName)

          if ('download' in document.createElement('a')) {
            // 非IE下载
            const elink = document.createElement('a')
            elink.download = fileName
            elink.style.display = 'none'
            elink.href = URL.createObjectURL(blob)
            document.body.appendChild(elink)
            elink.click()
            URL.revokeObjectURL(elink.href) // 释放URL 对象
            document.body.removeChild(elink)
          } else {
            // IE10+下载
            navigator.msSaveBlob(blob, fileName)
          }
          const successmsg = { riestcode: 0, riestmsg: 'success' }
          resolve(successmsg)
        }
      })
      .catch(fail => {
        reject(fail)
      })
  })
}
