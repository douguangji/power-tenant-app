/*
 * @Author: your name
 * @Date: 2020-06-20 09:55:48
 * @LastEditTime: 2020-08-07 18:15:23
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\auth.js
 */
import Cookies from 'js-cookie'

// const TokenKey = 'app_vue_member'

// export function getToken() {
//   return Cookies.get(TokenKey)
// }

// export function setToken(token) {
//   return Cookies.set(TokenKey, token)
// }

// export function removeToken() {
//   return Cookies.remove(TokenKey)
// }
// Cookies
export function SaveRiestClientDataCookies(dataname, data) {
  return Cookies.set(dataname, data)
}
export function GetRiestClientDataCookies(dataname) {
  return Cookies.get(dataname)
}
export function RemoveRiestClientDataCookies(dataname) {
  return Cookies.remove(dataname)
}

// LocalStorage
export function SaveRiestClientDataLocalStorage(dataname, data) {
  return localStorage.setItem(dataname, data)
}
export function GetRiestClientDataLocalStorage(dataname) {
  return localStorage.getItem(dataname)
}
export function RemoveRiestClientDataLocalStorage(dataname) {
  return localStorage.removeItem(dataname)
}

// SessionStorage
export function SaveRiestClientDataSessionStorage(dataname, data) {
  return sessionStorage.setItem(dataname, data)
}
export function GetRiestClientDataSessionStorage(dataname) {
  return sessionStorage.getItem(dataname)
}
export function RemoveRiestClientDataSessionStorage(dataname) {
  return sessionStorage.removeItem(dataname)
}

// // 所有的用户信息
// export function getUser() {
//   return GetRiestClientDataSessionStorage('userMessage')
// }
// export function setUser(token) {
//   return SaveRiestClientDataSessionStorage('userMessage', token)
// }
// export function removeUser() {
//   return RemoveRiestClientDataSessionStorage('userMessage')
// }
// // 登录返回的所有学校
// export function getschool() {
//   return GetRiestClientDataLocalStorage('userschool')
// }
// export function setschool(token) {
//   return SaveRiestClientDataLocalStorage('userschool', token)
// }
// export function removeschool() {
//   return RemoveRiestClientDataLocalStorage('userschool')
// }
// // 登录进去的学校
// export function gettentschoolid() {
//   return GetRiestClientDataSessionStorage('tentschoolid')
// }
// export function settentschoolid(token) {
//   return SaveRiestClientDataSessionStorage('tentschoolid', token)
// }
// export function removetentschoolid() {
//   return RemoveRiestClientDataSessionStorage('tentschoolid')
// }
// // 角色
// export function getrolestypeid() {
//   return GetRiestClientDataSessionStorage('rolestype')
// }
// export function setrolestypeid(token) {
//   return SaveRiestClientDataSessionStorage('rolestype', token)
// }
// export function removerolestypeid() {
//   return RemoveRiestClientDataSessionStorage('rolestype')
// }
// // 学校的身份
// export function getrole() {
//   return GetRiestClientDataSessionStorage('role')
// }
// export function setrole(token) {
//   return SaveRiestClientDataSessionStorage('role', token)
// }
// export function removerole() {
//   return RemoveRiestClientDataSessionStorage('role')
// }
