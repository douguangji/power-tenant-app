/*
 * @Author: your name
 * @Date: 2020-06-21 16:30:50
 * @LastEditTime: 2020-07-22 16:25:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\RiestDoorCallServer.js
 */
// import RiestBaseCallServer from '@/utils/RiestBaseCallServer'
import { RiestBaseCallServer } from './RiestBaseCallServer'
import { RiestBaseDownloadCallServer } from '@/utils/RiestBaseDownloadCallServer'
import riestswitch from './switch'
import { gettentschoolid, getrolestypeid } from './auth'

// post请求
export function RiestCallServerPost(req) {
  // console.log('a44444444', store.state.cententid);
  console.log('a++', riestswitch.member.RiestCallType)
  console.log('a++', riestswitch.member.CallClientType)
  console.log('a++', riestswitch.member.RiestBaseUrl)
  console.log('getrolestypeid()', getrolestypeid())
  req.data.RiestCallType = riestswitch.member.RiestCallType
  req.data.CallClientType = riestswitch.member.CallClientType
  req.data.TenantRequesallClientType
  req.data.TenantRequest = 1
  req.data.RiestBaseUrl = riestswitch.member.RiestBaseUrl
  req.data.tenantid = JSON.parse(gettentschoolid()).tenantid
  if (getrolestypeid()) {
    req.data.memberroleid = JSON.parse(getrolestypeid()).rolestypeid
  }
  // 请求类型
  req.data.RiestReqType = 101
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}
// 上传文件
export function RiestCallServerUpload(req) {
  req.data.RiestCallType = riestswitch.member.RiestCallType
  req.data.CallClientType = riestswitch.member.CallClientType
  req.data.RiestBaseUrl = riestswitch.member.RiestBaseUrl
  req.data.TenantRequest = 1
  req.data.tenantid = gettentschoolid()
  req.data.memberroleid = getrolestypeid()
  // 请求类型
  req.data.RiestReqType = 121
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}

// 字典数据请求
export function RiestCallServerDict(req) {
  // req.data.RiestCallType = 101
  req.data.RiestReqType = 111
  req.data.CallClientType = riestswitch.dict.CallClientType
  req.data.RiestBaseUrl = ''
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}

// 文件下载
export function RiestCallServerDownload(req) {
  req.data.RiestCallType = riestswitch.member.RiestCallType
  req.data.CallClientType = riestswitch.member.CallClientType
  req.data.RiestBaseUrl = riestswitch.member.RiestBaseUrl
  req.data.TenantRequest = 1
  req.data.tenantid = gettentschoolid()
  req.data.memberroleid = getrolestypeid()
  // 请求类型
  req.data.RiestReqType = 131

  return RiestBaseDownloadCallServer({
    url: req.url,
    method: 'post',
    data: req.data,
    responseType: 'blob'
  })
}
