import {
  SaveRiestClientDataCookies,
  GetRiestClientDataCookies,
  RemoveRiestClientDataCookies,
  SaveRiestClientDataLocalStorage,
  GetRiestClientDataSessionStorage,
  SaveRiestClientDataSessionStorage,
  RemoveRiestClientDataSessionStorage,
  GetRiestClientDataLocalStorage,
  RemoveRiestClientDataLocalStorage } from './RiestStorage'
const TokenKey = 'app_vue_member'
// 存储的token
export function getToken() {
  return GetRiestClientDataCookies(TokenKey)
}

export function setToken(token) {
  return SaveRiestClientDataCookies(TokenKey, token)
}

export function removeToken() {
  return RemoveRiestClientDataCookies(TokenKey)
}

// 存储的状态
export function gettablist() {
  return GetRiestClientDataCookies('tablist')
}

export function settablist(token) {
  return SaveRiestClientDataCookies('tablist', token)
}

export function removetablist() {
  return RemoveRiestClientDataCookies('tablist')
}

// 所有的用户信息
export function getUser() {
  return GetRiestClientDataSessionStorage('userMessage')
}
export function setUser(token) {
  return SaveRiestClientDataSessionStorage('userMessage', token)
}
export function removeUser() {
  return RemoveRiestClientDataSessionStorage('userMessage')
}
// 登录返回的所有学校
export function getschool() {
  return GetRiestClientDataLocalStorage('userschool')
}
export function setschool(token) {
  return SaveRiestClientDataLocalStorage('userschool', token)
}
export function removeschool() {
  return RemoveRiestClientDataLocalStorage('userschool')
}
// logo  lunbo
export function getdoormessage() {
  return GetRiestClientDataLocalStorage('DoorMessage')
}
export function setdoormessage(token) {
  return SaveRiestClientDataLocalStorage('DoorMessage', token)
}
export function removedoormessage() {
  return RemoveRiestClientDataLocalStorage('DoorMessage')
}
// 详情
export function getfance() {
  return GetRiestClientDataLocalStorage('fance')
}
export function setfance(token) {
  return SaveRiestClientDataLocalStorage('fance', token)
}
export function removefance() {
  return RemoveRiestClientDataLocalStorage('fance')
}
// 登录进去的学校
export function gettentschoolid() {
  return GetRiestClientDataSessionStorage('tentschoolid')
}
export function settentschoolid(token) {
  return SaveRiestClientDataSessionStorage('tentschoolid', token)
}
export function removetentschoolid() {
  return RemoveRiestClientDataSessionStorage('tentschoolid')
}
// 角色
export function getrolestypeid() {
  return GetRiestClientDataSessionStorage('rolestype')
}
export function setrolestypeid(token) {
  return SaveRiestClientDataSessionStorage('rolestype', token)
}
export function removerolestypeid() {
  return RemoveRiestClientDataSessionStorage('rolestype')
}
// 学校的身份
export function getrole() {
  return GetRiestClientDataSessionStorage('role')
}
export function setrole(token) {
  return SaveRiestClientDataSessionStorage('role', token)
}
export function removerole() {
  return RemoveRiestClientDataSessionStorage('role')
}
// 登录状态
export function getlogintype() {
  return GetRiestClientDataSessionStorage('logintype')
}
export function setlogintype(token) {
  return SaveRiestClientDataSessionStorage('logintype', token)
}
export function removelogintype() {
  return RemoveRiestClientDataSessionStorage('rologintypele')
}
