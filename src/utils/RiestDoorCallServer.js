/*
 * @Author: your name
 * @Date: 2020-06-21 16:30:50
 * @LastEditTime: 2020-07-22 16:25:15
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\RiestDoorCallServer.js
 */
import { RiestBaseCallServer } from './RiestBaseCallServer'
import { RiestBaseDownloadCallServer } from '@/utils/RiestBaseDownloadCallServer'
import riestswitch from './switch'

// post请求
export function RiestDoorCall(req) {
  console.log('a++', riestswitch.door.RiestCallType)
  console.log('a++', riestswitch.door.CallClientType)
  console.log('a++', riestswitch.door.RiestBaseUrl)
  req.data.RiestCallType = riestswitch.door.RiestCallType
  req.data.CallClientType = riestswitch.door.CallClientType
  req.data.RiestBaseUrl = riestswitch.door.RiestBaseUrl
  // 请求类型
  req.data.RiestReqType = 101

  // const systemparam = {
  //   // showsuccess > 0 显示成功弹框
  //   showsuccess: 0,
  //   // showerror > 0 显示失败弹框
  //   showerror: 0,
  //   // showloading > 0 显示loading遮罩层
  //   showloading: 0
  // }
  const data = { ...req.data }

  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: data
  })
}
// 上传文件
export function RiestDoorServerUpload(req) {
  req.data.RiestCallType = riestswitch.door.RiestCallType
  req.data.CallClientType = riestswitch.door.CallClientType
  req.data.RiestBaseUrl = riestswitch.door.RiestBaseUrl
  // 请求类型
  req.data.RiestReqType = 121
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}

// 字典数据请求
export function RiestCallServerDict(req) {
  // req.data.RiestCallType = 101
  req.data.RiestReqType = 111
  req.data.CallClientType = riestswitch.dict.CallClientType
  req.data.RiestBaseUrl = ''
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}
// 文件下载
export function RiestCallServerDownload(req) {
  req.data.RiestCallType = riestswitch.door.RiestCallType
  req.data.CallClientType = riestswitch.door.CallClientType
  req.data.RiestBaseUrl = riestswitch.door.RiestBaseUrl
  // 请求类型
  req.data.RiestReqType = 131

  return RiestBaseDownloadCallServer({
    url: req.url,
    method: 'post',
    data: req.data,
    responseType: 'blob'
  })
}
