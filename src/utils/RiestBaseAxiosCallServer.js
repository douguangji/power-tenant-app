import axios from 'axios'
import { RiestMessage } from '@/utils/message'
import { getToken } from '@/utils/auth'
// import { is_define } from '@/utils/auth'
// import store from '@/store/index'

import qs from 'qs' //  引入qs
//  create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_CALL, //  url = base url + request url
  withCredentials: true //  send cookies when cross-domain requests
  //  timeout: 600000 //  request timeout
})
//  request interceptor
service.interceptors.request.use(
  config => {
    /*
    sys param
      RiestCallType:  101 admin  111 tenant admin  121 door  131 pubmember  141 school member
      RiestReqType  101 normal post  111 dict   121 upload file   131 download file
      callclienttype: 101 pc web  111 mobile(以后分为 android,ios,weixin,手机浏览器,小程序等
      RiestBaseUrl: /admin  /pubmember等
    */
    console.log('【riest】 axios config url:' + config.url)
    console.log('【riest】 axios config data:' + qs.stringify(config.data))
    //  console.log('【riest】 axios config baseURL:' + config.baseURL)

    // const RiestCallType = config.data.RiestCallType
    const RiestReqType = config.data.RiestReqType
    const CallClientType = config.data.CallClientType
    const RiestBaseUrl = config.data.RiestBaseUrl
    //  判断为空，报错返回？

    const srcUrl = config.url
    // 旧代码特殊处理
    if (
      srcUrl.indexOf('/NewAdminPopedom.action') === 0 ||
      srcUrl.indexOf('/NewPopedom.action') === 0
    ) {
      config.url = RiestBaseUrl + process.env.VUE_APP_BASE_CALL_COMMON + srcUrl
    } else {
      switch (RiestReqType) {
        case 101:
          config.timeout = 30000
          config.url =
            RiestBaseUrl + process.env.VUE_APP_BASE_CALL_NORMAL + srcUrl
          break
        case 111:
          config.timeout = 30000
          config.url =
            RiestBaseUrl + process.env.VUE_APP_BASE_CALL_DICT + srcUrl
          break
        case 121:
          config.timeout = 600000
          config.url =
            RiestBaseUrl + process.env.VUE_APP_BASE_CALL_NORMAL + srcUrl
          break
        case 131:
          config.timeout = 600000
          config.url =
            RiestBaseUrl + process.env.VUE_APP_BASE_CALL_NORMAL + srcUrl
          break
      }
    }

    const jwttoken = getToken()

    //  判断请求的类型：如果是post请求就把默认参数拼到data里面；如果是get请求就拼到params里面
    if (RiestReqType === 121) {
      console.log('req type : upload file')
      config.data.set('CallClientType', CallClientType)
      config.data.set('jwttoken', jwttoken)
      //  return config
    } else if (config.method === 'post') {
      console.log('req type : post')
      config.data = qs.stringify({
        CallClientType: CallClientType,
        jwttoken: jwttoken,
        ...config.data
      })
    } else if (config.method === 'get') {
      console.log('req type : get')
      config.params = {
        CallClientType: CallClientType,
        jwttoken: jwttoken,
        ...config.params
      }
    }

    console.log('【riest】 after axios config url:' + config.url)
    console.log('【riest】 after axios config baseURL:' + config.baseURL)

    return config
  },
  error => {
    //  do something with request error
    console.log(error) //  for debug
    return Promise.reject(error)
  }
)
//  在请求数据前修改请求数据（添加系统参数CallClientType）
//  eslint-disable-next-line no-return-assign
/*
service.defaults.transformRequest = [data => {
  qs.stringify(data) + '&CallClientType=111' + '&jwttoken=' + getToken()
}
]
*/
//  response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */
  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    return response
  },
  error => {
    console.log('err' + error) //  for debug

    RiestMessage('网络错误，请稍后再试', 'error')
    return Promise.reject(error)
  }
)

export default service
