/*
 * @Author: your name
 * @Date: 2020-06-20 16:28:57
 * @LastEditTime: 2020-07-27 15:12:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\RiestOpenPage.js
 */
function RiestOpenPage(nativehtmlurl, paramjson, newwin) {
  // newwin类型
  // 1 replace 无记录,返回上上个页面
  // 2  push    有记录  返回上一个记录
  switch (newwin) {
    case 1:
      this.$router.replace({
        path: nativehtmlurl,
        query: paramjson
      })
      // 跳转到指定url路径，但是history栈中不会有记录，点击返回会跳转到上上个页面 (就是直接替换了当前页面)
      break
    case 2:
      this.$router.push({
        path: nativehtmlurl,
        query: paramjson
      })
      // 跳转到指定url路径，并想history栈中添加一个记录，点击后退会返回到上一个页面
      break
    default:
      this.$router.push({
        path: nativehtmlurl,
        query: paramjson
      })
      // 跳转到指定url路径，并想history栈中添加一个记录，点击后退会返回到上一个页面
      break
  }
}

export default {
  RiestOpenPage
}
