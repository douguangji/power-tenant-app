/*
 * @Author: your name
 * @Date: 2020-06-20 20:31:24
 * @LastEditTime: 2020-08-07 12:06:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\RiestCallServer.js
 */
import RiestBaseAxiosCallServer from '@/utils/RiestBaseAxiosCallServer'
import { RiestMessage, RiestConfirm, RiestNotification } from '@/utils/message'
// import store from '@/store'
import RiestUtil from '@/utils/riestutil'
import { Toast } from 'vant'
import riestswitch from '@/utils/switch'
// import {
//   removeToken,
//   removeUser,
//   removeschool,
//   removetentschool,
//   removerolestypeid,
//   removerole
// } from './../utils/auth'
import { AppTokenLogin } from '@/loginAPI/tokenlogin.js'

export function RiestBaseCallServer(req) {
  console.log('req', req.data)
  const showloading = req.data.showloading
  if (!RiestUtil.is_define(showloading) || showloading > 0) {
    Toast.loading({
      message: '加载中...',
      forbidClick: true,
      loadingType: 'spinner'
    })
  }

  return new Promise((resolve, reject) => {
    RiestBaseAxiosCallServer({
      url: req.url,
      method: req.method,
      data: req.data,
      responseType: req.responseType || ''
    }).then(response => {
      Toast.clear()
      if (!RiestUtil.is_define(showloading) || showloading > 0) {
        Toast.clear()
      }
      const res = response.data

      // 文件下载，单独处理
      if (req.data.RiestReqType === 131) {
        resolve(response)
        return
      }

      // 请求响应弹框
      BaseCallShowMsg(req, res, success => {
        resolve(success)
      }, fail => {
        reject(fail)
      })
    }).catch(() => {
      if (!RiestUtil.is_define(showloading) || showloading > 0) {
        Toast.clear()
      }
      const message = { riestcode: 1, riestmsg: '网络中断' }
      reject(message)
    })
  })
}

export function BaseCallShowMsg(req, res, success, fail) {
  const showsuccess = req.data.showsuccess
  const showerror = req.data.showerror

  console.log('a1', res)
  if (!RiestUtil.is_define(res.riestcode)) {
    RiestMessage('返回参数riestcode[错误信息]未定义', 'error')
    return
  }
  if (!RiestUtil.is_define(res.riestmsg)) {
    RiestMessage('返回参数riestmsg[错误信息]未定义', 'error')
    return
  }
  console.log('is_defin', res)
  if (res.riestcode !== 0) {
    if (res.riestcode === 9999) {
      //  to re-login
      // 失效后自动重新登录
      AppTokenLogin()
      fail(res)
      // RiestConfirm(
      //   '您已经注销，您可以取消以停留在此页面，或再次登录',
      //   '登录已过期',
      //   '重新登录',
      //   '取消',
      //   'warning'
      // ).then(() => {
      //   // 需要修改重新登录的跳转方法
      //   //  请求token刷新的接口也要修改
      //   // store.dispatch('user/resetToken').then(() => {
      //   //   location.reload()
      //   // })
      //   removeToken()
      //   removeUser()
      //   removeschool()
      //   removetentschool()
      //   removerolestypeid()
      //   removerole()
      //   location.reload()
      //   this.$router.push({ path: '/' })
      // })
    } else if (res.riestcode === 9998) {
      //  to re-login
      const msg = '您未配置权限：' + res.riestmsg
      RiestConfirm(msg, '确认错误', '我知道了', '取消', 'warning')
      // RiestMessage(msg, 'error')
      fail(res)
    } else if (res.riestcode === 7999) {
      //  to re-login
      const msg = res.riestmsg
      RiestMessage(msg, 'error')
      fail(res)
    } else if (res.riestcode === 7995) {
      //  to re-login
      const msg = res.riestmsg
      RiestMessage(msg, 'error')
      fail(res)
    } else {
      console.log('....', res, showerror)
      // 如果页面传值showerror不为空时,弹出message
      if (RiestUtil.is_define(showerror)) {
        console.log('a1', res, showerror)
        if (showerror > 0) {
          RiestMessage(res.riestmsg, 'error')
        }
      } else {
        console.log('a2a2a2', riestswitch.DefaultErrorShow, res, showerror)
        if (riestswitch.DefaultErrorShow === 1) {
          RiestMessage(res.riestmsg, 'error')
        }
      }
      console.log('res', res)
      fail(res)
    }
  } else {
    console.log('showsuccess1', res, riestswitch.DefaultSuccessShow)
    console.log('showsuccess1-showsuccess', showsuccess)
    if (RiestUtil.is_define(showsuccess)) {
      console.log('show', res)
      if (showsuccess > 0) {
        RiestNotification(res.riestmsg, 'success')
      }
    } else {
      console.log('else', res, riestswitch.DefaultSuccessShow)
      if (riestswitch.DefaultSuccessShow === 1) {
        console.log('else1', res)
        // RiestNotification(res.riestmsg, 'success')
      }
    }
    console.log('showsuccess2', res)
    success(res)
    //   return
  }
}
