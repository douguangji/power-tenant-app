/*
 * @Author: your name
 * @Date: 2020-06-21 16:37:03
 * @LastEditTime: 2021-03-23 13:52:10
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\switch.js
 */

// 定义公共的请求头参数
const DefaultErrorShow = 1
const DefaultSuccessShow = 1
/*
    sys param
      RiestCallType:  101 admin  111 tenant admin  121 door  131 pubmember  141 school member
      RiestReqType  101 normal post  111 dict   121 upload file
      callclienttype: 101 pc web  111 mobile(以后分为 android,ios,weixin,手机浏览器,小程序等
      RiestBaseUrl: /admin  /pubmember等
    */
const door = {
  RiestCallType: '121',
  //   RiestReqType : '101',
  CallClientType: '111',
  RiestBaseUrl: '/door'
}
const pubmember = {
  RiestCallType: '131',
  //   RiestReqType : '101',
  CallClientType: '111',
  RiestBaseUrl: '/customer/power'
}
const member = {
  RiestCallType: '141',
  //   RiestReqType : '101',
  CallClientType: '111',
  RiestBaseUrl: '/pubmember/school'
}
const dict = {
  // RiestCallType: '101',
  //   RiestReqType : '101',
  CallClientType: '111'
  // RiestBaseUrl: '/common'
}

const NowPage = 'Home'
export default {
  DefaultErrorShow,
  DefaultSuccessShow,
  door,
  pubmember,
  member,
  dict,
  NowPage
}
