/* eslint-disable no-trailing-spaces */
/*
 * @Author: your name
 * @Date: 2020-06-05 10:36:46
 * @LastEditTime: 2020-08-07 18:14:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\message.js
 // eslint-disable-next-line no-trailing-spaces
 */
/* eslint-disable no-multi-spaces */
import { Toast } from 'vant' // 页面中轻量提示
import { Notify } from 'vant'
import { Dialog } from 'vant'
/**
 * 通知形式
 * @param {message} string 正文内容
 * @param {icon} string 类型
 *  icon可选：success 成功||error 错误||warning 警告
 */
export function RiestMessage(messageinfo, typeinfo) {
  Toast({
    message: messageinfo,
    forbidClick: true,
    loadingType: 'spinner',
    icon:
      typeinfo === 'warning'
        ? 'fail'
        : typeinfo === 'error'
          ? 'cross'
          : typeinfo || null
  })
}
/**
 * 通知形式
 * @param {title} string 标题
 * @param {message} string 正文内容
 * @param {type} 类型
 *  type可选：success 成功||error 错误||warning 警告
 */
export function RiestNotification(messageinfo, typeinfo) {
  Notify({
    type: typeinfo === 'error' ? 'danger' : typeinfo || 'primary',
    message: messageinfo,
    duration: 3000
  })
}
/**
 * 通知形式
 * @param {title} string 标题
 * @param {text} string 正文内容
 * @param {done} string 确认按钮文本
 */
export function RiestAlert(text, title, done) {
  Dialog.alert({
    title: title,
    message: text,
    confirmButtonText: done
  })
}
/**
 * 通知形式
 * @param {title} string 标题
 * @param {text} string 正文内容
 * @param {done} string 确认按钮文本
 * @param {close} string 取消按钮文本
 */
export function RiestConfirm(text, title, done, close) {
  Dialog.confirm({
    title: title,
    message: text,
    confirmButtonText: done,
    cancelButtonText: close
  })
}
