
import sw from './switch.js'
import { RiestMessage } from './message.js'

function exit_app() {
  window.api.closeWidget({
    id: window.api.appId,
    retData: {
      name: 'closeWidget'
    },
    animation: {
      type: 'flip',
      subType: 'from_bottom',
      duration: 500
    },
    silent: true
  })
}
// 退出App
var clickexitnum = 0

export function android_exit() {
  console.log('【trace】:  bind android exit')

  window.api.addEventListener({
    name: 'keyback'
  },
  function(ret, err) {
    console.log('【trace】:  trigger keyback')

    if (clickexitnum === 0) {
      if (sw.NowPage === 'Home') {
        clickexitnum = 1

        RiestMessage('再按一次返回键退出')
        // RiestShowErrorCallBack('successtoast', '再按一次返回键退出')
      }
      setTimeout(function() {
        exitclickcancel()
      }, 3 * 1000)

      window.history.back()
      return
    }

    clickexitnum = 0
    // ue_script('index.html','Exit_app()');
    if (sw.NowPage === 'Home') {
      exit_app()
    }
  })
}

function exitclickcancel() {
  clickexitnum = 0
}
