/*
 * @Author: your name
 * @Date: 2020-06-21 16:30:50
 * @LastEditTime: 2020-07-22 16:25:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \YongLaw_Vue_Admin\src\utils\RiestDoorCallServer.js
 */
// import RiestBaseCallServer from '@/utils/RiestBaseCallServer'
import { RiestBaseCallServer } from './RiestBaseCallServer'
import { RiestBaseDownloadCallServer } from '@/utils/RiestBaseDownloadCallServer'
import riestswitch from './switch'

// post请求
export function RiestCallServerPost(req) {
  console.log('a++', riestswitch.pubmember.RiestCallType)
  console.log('a++', riestswitch.pubmember.CallClientType)
  console.log('a++', riestswitch.pubmember.RiestBaseUrl)
  req.data.RiestCallType = riestswitch.pubmember.RiestCallType
  req.data.CallClientType = riestswitch.pubmember.CallClientType
  req.data.RiestBaseUrl = riestswitch.pubmember.RiestBaseUrl
  // 请求类型
  req.data.RiestReqType = 101
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}
// 上传文件
export function RiestCallServerUpload(req) {
  req.data.RiestCallType = riestswitch.pubmember.RiestCallType
  req.data.CallClientType = riestswitch.pubmember.CallClientType
  req.data.RiestBaseUrl = riestswitch.pubmember.RiestBaseUrl
  // 请求类型
  req.data.RiestReqType = 121
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}

// 字典数据请求
export function RiestCallServerDict(req) {
  // req.data.RiestCallType = 101
  req.data.RiestReqType = 111
  req.data.CallClientType = riestswitch.dict.CallClientType
  req.data.RiestBaseUrl = ''
  return RiestBaseCallServer({
    url: req.url,
    method: 'post',
    data: req.data
  })
}

// 文件下载
export function RiestCallServerDownload(req) {
  req.data.RiestCallType = riestswitch.pubmember.RiestCallType
  req.data.CallClientType = riestswitch.pubmember.CallClientType
  req.data.RiestBaseUrl = riestswitch.pubmember.RiestBaseUrl
  // 请求类型
  req.data.RiestReqType = 131

  return RiestBaseDownloadCallServer({
    url: req.url,
    method: 'post',
    data: req.data,
    responseType: 'blob'
  })
}
