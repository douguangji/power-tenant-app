/*
 * @Author: your name
 * @Date: 2020-07-22 11:41:26
 * @LastEditTime: 2021-03-19 18:49:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \memberapp\src\main.js
 */

import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './../static/styles/reset.css'
import './../static/styles/border.css'
import fastClick from 'fastclick'
import 'vant/lib/index.css'
import Vant from 'vant'
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

import sw from './utils/switch.js'
import { android_exit } from './utils/exitapp.js'
import { AppTokenLogin } from '@/loginAPI/tokenlogin.js'
import { getlogintype, getToken } from './utils/auth'

Vue.config.productionTip = false
fastClick.attach(document.body)
Vue.use(Vant)

router.beforeEach((to, from, next) => {
  console.log(sw.NowPage, to.matched[0].name)
  sw.NowPage = to.matched[0].name
  console.log('to', to, getToken())
  if (to.matched.some(record => record.meta.requireAuth)) {
    if (getToken()) { // 缓存在是否存在token
      next()
    } else {
      next({
        path: '/login',
        query: { redirect: to.fullPath } // 将跳转的路由path作为参数，登录成功后跳转到该路由

      })
    }
  } else {
    next()
  }
})
router.afterEach((to, from, next) => {
  document.body.scrollTop = 0
  document.documentElement.scrollTop = 0
})
const VueRunInApiCloud = process.env.VUE_APP_RunInApiCloud
console.log('VueRunInApiCloud = ', JSON.stringify(VueRunInApiCloud))
if (parseInt(VueRunInApiCloud) === 1) {
  console.log('使用apicloud加载')
  window.apiready = function() {
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
    // 这个方法属于APP内初始化退出应用
    android_exit()
    // 初始化完成后直接使用token登录
    if (!getlogintype() || getlogintype() !== 1) {
      AppTokenLogin()
    }
  }
} else {
  console.log('使用vue加载')
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
  // 初始化完成后直接使用token登录
  if (!getlogintype() && getlogintype() !== 1) {
    AppTokenLogin()
  }
}

